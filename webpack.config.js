module.exports = {
  entry: ['babel-polyfill', './src/index'],
  output: {
    filename: './build/bundle.js',
  },
  module: {
    loaders: [
      { test: /.jsx?$/, loader: 'babel-loader', exclude: 'node_modules' },
    ],
  },
  devServer: {
    contentBase: './public',
    port: '3000',
  },
};
