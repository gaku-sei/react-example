import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';

import Application from '../src/Application';

describe('Application', () => {
  it('should contain Hello World!', () => {
    const application = shallow(<Application />);

    expect(application.text()).to.equal('Hello World!');
  });
});
